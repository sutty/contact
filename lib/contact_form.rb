class ContactForm
  ROOT_PATH = File.expand_path('.').freeze

  class << self

    # Este archivo es una lista de dominios a los que estamos permitiendo
    # enviar correo
    def authorized_hosts
      @authorized_hosts ||= File.read(authorized_hosts_file).split("\n")
    end

    # Emula una tabla de direcciones virtuales de postfix
    #
    # @return { 'hostname' => [ 'address', 'address2' ] }
    def virtual
      @virtual ||= Hash[File.read(virtual_file).split("\n").map do |line|
        fields = line.split(/,?\s+/)

        [fields.shift.strip, fields.map(&:strip)]
      end]
    end

    # TODO: Contactar a la API de Sutty
    def allowed?(hostname)
      authorized_hosts.include? hostname
    end

    def authorized_hosts_file
      File.join(ROOT_PATH, 'authorized_hosts')
    end

    def virtual_file
      File.join(ROOT_PATH, 'virtual')
    end
  end
end
