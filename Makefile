all: build save

build:
	docker build -t sutty/contact .

save:
	docker save sutty/contact:latest | gzip | ssh root@anarres.sutty.nl docker load
