# Copyright (C) 2018 f <f@sutty.nl>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Affero General Public License for more details.
# 
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <https://www.gnu.org/licenses/>.

require 'sinatra'
require 'pony'
require_relative 'lib/contact_form'

# Oh no
set :protection, false

get '/' do
  send_file File.join(ContactForm::ROOT_PATH, 'README.md'),
            type: 'text/plain; charset=utf-8'
end

post '/' do
  host = params.fetch(:site, 'sutty.nl')
  from = params.fetch(:from, "noreply@#{host}")
  pronouns = params.dig(:pronouns)

  to   = "contacto@#{host}"
  site = "https://#{host}"

  redirect_url = params.fetch(:redirect, site)

  # Redirigir a una gzip bomb si el sitio no está autorizado
  unless ContactForm.allowed? host
    headers 'Content-Encoding' => 'gzip'
    send_file 'bomb.gz', type: 'text/html'
    halt
  end

  subject = "[#{site}] #{params.dig(:subject)}"
  body    = <<~EOM
    Nombre: #{params.dig(:name)} (#{pronouns})
    Contacto: #{params.dig(:contact)}
    Consentimiento: #{params.dig(:gdpr)}

    #{params.dig(:body)}
  EOM

  # Enviamos localmente en lugar de hacernos pasar por la persona que
  # contacta.  La respuesta llega donde corresponde.
  Thread.new do
    Pony.mail to: ContactForm.virtual.fetch(host, to),
      from: to,
      reply_to: from,
      via: :smtp,
      via_options: {
        address: 'postfix',
        domain: 'sutty.nl',
        enable_starttls_auto: false
      },
      body: body,
      subject: subject
  end

  redirect redirect_url
end
