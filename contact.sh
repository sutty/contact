#!/bin/sh
set -e

pidfile="/tmp/contact.pid"

case $1 in
  stop) cat "${pidfile}" | xargs kill ;;
  *)
    cd /srv/http
    bundle exec \
      rackup --env production \
             --pid "${pidfile}" \
             --daemonize \
             --server puma ;;
esac
