FROM alpine:3.12 as build
MAINTAINER "f <f@sutty.nl>"
ENV RACK_ENV production

RUN apk add --no-cache tzdata ruby ruby-json ruby-bigdecimal git

# https://github.com/rubygems/rubygems/issues/2918
# https://gitlab.alpinelinux.org/alpine/aports/issues/10808
COPY ./rubygems-platform-musl.patch /tmp/
RUN apk add --no-cache patch
RUN cd /usr/lib/ruby/2.7.0 && patch -Np 0 -i /tmp/rubygems-platform-musl.patch

# Agregar el usuario
RUN addgroup -g 82 -S www-data
RUN adduser -s /bin/sh -G www-data -h /home/app -D app
RUN install -dm750 -o app -g www-data /home/app/contact
RUN gem install --no-document bundler

USER app
WORKDIR /home/app/contact

COPY --chown=app:www-data ./Gemfile .
COPY --chown=app:www-data ./Gemfile.lock .
RUN bundle install --path=./vendor --without="test development"
RUN rm vendor/ruby/2.7.0/cache/*.gem

COPY --chown=app:www-data ./.git/ ./.git/
RUN cd .. && git clone contact checkout

WORKDIR /home/app/checkout
RUN mv ../contact/vendor ./vendor
RUN mv ../contact/.bundle ./.bundle

FROM sutty/monit:latest
ENV RACK_ENV production

RUN apk add --no-cache tzdata ruby ruby-json ruby-bigdecimal ruby-etc
RUN addgroup -g 82 -S www-data
RUN adduser -s /bin/sh -G www-data -h /srv/http -D app
RUN gem install --no-document bundler

# https://github.com/rubygems/rubygems/issues/2918
# https://gitlab.alpinelinux.org/alpine/aports/issues/10808
COPY ./rubygems-platform-musl.patch /tmp/
RUN apk add --no-cache patch && cd /usr/lib/ruby/2.7.0 && patch -Np 0 -i /tmp/rubygems-platform-musl.patch && apk del patch

USER app
WORKDIR /srv/http

COPY --from=build --chown=app:www-data /home/app/checkout /srv/http
COPY ./virtual /srv/http/virtual
COPY ./authorized_hosts /srv/http/authorized_hosts

USER root
RUN install -m 640 -o root -g root ./monit.conf /etc/monit.d/contact.conf
RUN install -m 755 -o root -g root ./contact.sh /usr/local/bin/contact

EXPOSE 9292
