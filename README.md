> Copyright (C) 2018 f <f@sutty.nl>
> 
> This program is free software: you can redistribute it and/or modify
> it under the terms of the GNU Affero General Public License as
> published by the Free Software Foundation, either version 3 of the
> License, or (at your option) any later version.
> 
> This program is distributed in the hope that it will be useful, but
> WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
> Affero General Public License for more details.
> 
> You should have received a copy of the GNU Affero General Public
> License along with this program.  If not, see
> <https://www.gnu.org/licenses/>.

Código fuente: https://0xacab.org/sutty/contact-form

# Formulario de contacto

Un formulario de contacto simple para acompañar sitios estáticos.

# El formulario

Crea un formulario de contacto que envíe los siguientes datos:

* `site` el dominio desde donde se envía el correo (ej. `sutty.nl`)
* `name` el nombre de la persona que escribe
* `pronouns` los pronombres preferidos
* `gdpr` si la persona aceptó los términos de privacidad o no
* `from` la dirección de correo
* `contact` otras formas de contacto
* `body` el cuerpo del mensaje
* `redirect` la URL a donde se redirige luego del envío (pantalla de
  agradecimiento, etc.)

# La configuración

Crea un archivo `authorized_hosts` e introduce un dominio por cada
línea.  Este va a ser el listado de dominios a los que permitimos enviar
correo.

```bash
echo sutty.nl >> authorized_hosts
```

Crea un segundo archivo `virtual` y por cada línea introduce `dominio
direccion`.

```bash
echo sutty.nl hi@sutty.nl >> virtual
```

Con estas dos cosas, `contact-form` verifica que el valor `site` que
envía el formulario de contacto esté incluido en nuestro listado.
Luego, es capaz de enviarlo a cualquier dirección que le digamos.

Tip: Como en `virtual(5)` de `postfix`, podemos enviar copias a varias
direcciones con el formato:

```
sutty.nl f@sutty.nl,j@sutty.nl, etc@dominio.org
```
